# EXAMPLE - HTTP REQUEST TO API NAICS IDENTIFIER

## Getting Started

Following these instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Java SDK/JRE you need to run the software:

|  Sofware   | Version |
| :--------: | :-----: |
|    Jdk     |   11    |
|    Jre     |   11    |

## Usage

In order to run the process, make sure that you have the proper token to send requests. Once you have the token, follow the next steps to execute the code:

1. Replace '**YOUR_TOKEN_HERE**' with your **token**
2. Compile and run the code

**ATTENTION**: It is necessary to compile and run the code with an IDE to view the results in console.

## Built With

* [IntelliJ](https://www.jetbrains.com/idea/) - The IDE for JVM used
* [Java](https://www.java.com/en/) - Programming language
* [Github](https://github.com/) - Code Hosting Platform for Version Control

## Authors

* **Raul Lazaro Velasco** - *Developer* - lazaro@relativity6.com

## License

This project is private, owned by "Relativity6"